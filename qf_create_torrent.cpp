// clang-format off

// Install packages "build-essential libboost-all-dev libssl-dev"

// cd /tmp')
// wget https://github.com/arvidn/libtorrent/archive/libtorrent_1_2_0.zip')
// unzip libtorrent_1_2_0.zip
// cd libtorrent-libtorrent_1_2_0
// mkdir build
// cd build
// cmake ..
// make -j 4
// su -c "make install"

// cd PATH WITH "qf_create_torrent.cpp" FILE
// g++ -std=c++17 -pedantic -Wall -Wextra -Wconversion -Wsign-conversion qf_create_torrent.cpp -o qf_create_torrent -Wl,-rpath,/usr/local/lib -ltorrent-rasterbar -lboost_system
// su -c "cp qf_create_torrent /usr/local/bin"

// clang-format on

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include <libtorrent/create_torrent.hpp>

bool starts_with(std::string const &a, std::string const &b) {
  if (b.size() > a.size()) {
    return false;
  }
  for (size_t i = 0; i < b.size(); ++i) {
    if (a[i] != b[i]) {
      return false;
    }
  }
  return true;
}

int main(int argc, char *argv[]) {

  auto display_help = [&argv] {
    std::cout << "Usage: " << argv[0] << " <file|directory>"
              << " <-a URL> [-a URL]*"
              << " [-o output]"
              << " [-s|--piece_size 1|2|4|8|16]"
              << " [--mutable]"
              << " [-h|--help|-?]"
              << " [-v|--version]" << std::endl;
  };

  using namespace std::string_literals;

  std::vector<std::string> args(argv, argv + argc);

  bool const version =
      std::find(args.begin(), args.end(), "-v"s) != args.end() ||
      std::find(args.begin(), args.end(), "--version"s) != args.end();
  if (version) {
    std::cout << argv[0] << " 1.0" << std::endl;
    return 0;
  }

  bool const help =
      std::find(args.begin(), args.end(), "-h"s) != args.end() ||
      std::find(args.begin(), args.end(), "--help"s) != args.end() ||
      std::find(args.begin(), args.end(), "-?"s) != args.end();
  if (help) {
    display_help();
    return 0;
  }

  if (argc < 4) {
    display_help();
    return 1;
  }

  // TODO: clean it
  std::vector<std::string> const announces = [&]() {
    std::vector<std::string> r;

    for (int i = 1; i < argc; ++i) {
      if (argv[i] == "-a"s) {
        if (argc == i) {
          // TODO: clean it
          std::cerr << "Error: missing URL after -a option" << std::endl;
          display_help();
          exit(1);
        } else if (starts_with(argv[i + 1], "https://") == false &&
                   starts_with(argv[i + 1], "http://") == false) {
          // TODO: clean it
          std::cerr << "Error: URL after -a option does not begin by https:// "
                       "or http://"
                    << std::endl;
          display_help();
          exit(1);
        } else {
          r.emplace_back(argv[i + 1]);
        }
      }
    }
    return r;
  }();
  if (announces.empty()) {
    std::cerr << "Error: Missing \"-a URL\" option" << std::endl;
    display_help();
    return 1;
  }

  std::string const input_file = args[1];

  std::string const output_file = [&]() {
    auto it = std::find(args.begin(), args.end(), "-o"s);
    if (it == args.end() || it == args.end() - 1) {
      it = std::find(args.begin(), args.end(), "--output"s);
    }
    if (it == args.end() || it == args.end() - 1) {
      return input_file.substr(input_file.rfind('/') == std::string::npos
                                   ? 0
                                   : input_file.rfind('/') + 1,
                               std::string::npos) +
             ".torrent";
    }
    return *(it + 1);
  }();

  int const piece_size = [&]() {
    auto it = std::find(args.begin(), args.end(), "-s"s);
    if (it == args.end() || it == args.end() - 1) {
      it = std::find(args.begin(), args.end(), "--piece_size"s);
    }
    if (it == args.end() || it == args.end() - 1) {
      // Try to compute piece_size depending of the size of the torrent?
      // I think 16 Mio is a good value for all torrents
      return 16;
    }
    std::string const s = *(it + 1);
    if (s == "1" || s == "2" || s == "4" || s == "8" || s == "16") {
      return std::stoi(s);
    } else {
      // TODO: clean it
      std::cerr << "Error: piece_size is not 1 or 2 or 4 or 8 or 16"
                << std::endl;
      display_help();
      exit(1);
    }
  }() * 1024 * 1024;

  libtorrent::create_flags_t flags = {};

  flags |= libtorrent::create_torrent::optimize_alignment;

  bool const mutable_torrent =
      std::find(args.begin(), args.end(), "--mutable"s) != args.end();
  if (mutable_torrent) {
    flags |= libtorrent::create_torrent::mutable_torrent_support;
  }

  std::cout << "Create torrent of \"" << input_file << "\"" << std::endl;
  std::cout << std::endl;
  std::cout << "- Output file:     "
            << "\"" << output_file << "\"" << std::endl;
  std::cout << "- Pieces size:     " << piece_size << std::endl;
  std::cout << "- Mutable torrent: " << std::boolalpha << mutable_torrent
            << std::endl;
  std::cout << "- Annonces:" << std::endl;
  for (std::string const &announce : announces) {
    std::cout << "    " << announce << std::endl;
  }
  std::cout << std::endl;

  // https://www.libtorrent.org/reference-Create_Torrents.html

  libtorrent::file_storage fs;

  // Recursively adds files in directories
  libtorrent::add_files(fs, input_file);

  libtorrent::create_torrent t(fs // file_storage &
                               ,
                               piece_size // int piece_size = 0
                               ,
                               -1 // int pad_file_limit = -1
                               ,
                               flags // int flags = optimize_alignment
                               ,
                               -1 // int alignment = -1
  );

  t.set_priv(true);

  for (std::string const &announce : announces) {
    t.add_tracker(announce);
  }

  // t.set_creator("libtorrent example");

  // Reads the files and calculates the hashes
  libtorrent::set_piece_hashes(t, ".");

  std::ofstream out(output_file, std::ios_base::binary);
  libtorrent::bencode(std::ostream_iterator<char>(out), t.generate());

  return 0;
}

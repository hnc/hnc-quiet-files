#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import hashlib
import subprocess
import datetime
import shutil

# import zipfile
from PIL import Image

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "..", "sin-python", "src"
    )
)
import sin.fs
import sin.term

software = "Quiet Files"
version = "1.0"

# Common


def merge_two_dicts(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    """https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression"""
    z = x.copy()
    if len(y) != 0:
        z.update(y)
    return z


# .md files


def md_get_values_of_key(md_path, key):
    r = []
    file = open(md_path, "r")
    for line in file:
        if line.startswith("`" + key + "` "):
            r += [line[len("`" + key + "` ") : -1]]
    file.close()
    return r


# def md_key_value(line, key):
# if line.startswith(key):
# return { key: line[len(key + ' = '):-1] }
# else:
# return { }

# def md_read_file(path):
# r = { }
# file = open(path, 'r')
# for line in file:
# r = merge_two_dicts(r, get_md_value(line, 'title_en'))
# r = merge_two_dicts(r, get_md_value(line, 'title_fr'))
# r = merge_two_dicts(r, get_md_value(line, 'year'))
# r = merge_two_dicts(r, get_md_value(line, 'genre'))
# r = merge_two_dicts(r, get_md_value(line, 'filename'))
# file.close()
# return r

# Quiet file


def quiet_files_check_tag(quiet_files_dir, path, md_path):
    full_path = os.path.join(quiet_files_dir, path)
    filenames = os.listdir(os.path.join(quiet_files_dir, path))
    try:
        filenames.remove(path + ".md")
        filenames.remove("nfo")
    except:
        pass
    filenames.sort()
    print("")

    # Check type and tags
    print(sin.term.blue("Check tags"))
    print("")

    # Get type
    type = ""
    md = open(md_path).read()
    md_lines = md.split("\n")
    for md_line in md_lines:
        if md_line.startswith("`Type` "):
            type = md_line[7:]
            print('Type is "' + type + '"')
            break
    if type == "":
        print(sin.term.tag_error(), "Type not found in .md file")
    print("")

    # Check tags
    path_tags = ""
    if path.rfind("[") == -1:
        print(sin.term.tag_error(), "Tags in path are missing (e.g. [CODEC, Size Mio]")
    else:
        path_tags = path[path.rfind("[") :]
        print("Tags are", path_tags)

    # Check codec
    if type.startswith("Software"):
        if path_tags.find("GNU-Linux") == -1 and path_tags.find("Windows") == -1:
            print(sin.term.tag_error(), 'Missing tag "GNU-Linux" or "Windows"')
    else:
        codec_tags = []
        for filename in filenames:
            filename_path = os.path.join(full_path, filename)
            if (
                os.path.isdir(filename_path)
                or (" - Trailer " in filename)
                or (" - Trailer." in filename)
                or (" - Teaser " in filename)
            ):
                print('- Ignore "' + filename + '" to check the tags')
                continue
            else:
                print('- Use "' + filename + '" to check the tags')
            output = subprocess.check_output(
                'mediainfo "' + filename_path + '"', shell=True
            )
            output_lines = output.decode().split("\n")
            codec_tag = "UNKNOWN"
            i = 0
            while i < len(output_lines):
                if output_lines[i] == "General":
                    while output_lines[i] != "":
                        i = i + 1
                if output_lines[i].startswith("Format  "):
                    format = output_lines[i].replace(
                        "Format                                   : ", ""
                    )
                    if format == "AVC":
                        while i < len(output_lines):
                            if output_lines[i].startswith("Codec ID"):
                                codec_id = output_lines[i].replace(
                                    "Codec ID                                 : ", ""
                                )
                                codec_tag = codec_id
                                break
                            i = i + 1
                    elif format == "MPEG-4 Visual":
                        while i < len(output_lines):
                            if output_lines[i].startswith("Codec ID/Hint "):
                                codec_id = output_lines[i].replace(
                                    "Codec ID/Hint                            : ", ""
                                )
                                codec_tag = codec_id
                                if codec_tag == "DivX 3 Low":
                                    codec_tag = "DivX 3"
                                break
                            i = i + 1
                    elif format == "MPEG Audio":
                        while i < len(output_lines):
                            if output_lines[i].startswith("Format profile "):
                                format_profile = output_lines[i].replace(
                                    "Format profile                           : ", ""
                                )
                                if format_profile == "Layer 3":
                                    codec_tag = "MP3"
                                break
                            i = i + 1
                    elif format == "AAC LC" or format == "AAC LC SBR":
                        codec_tag = "AAC"
                    elif format == "E-AC-3 JOC":
                        codec_tag = "E-AC-3"
                    elif (
                        format == "DTS XLL"
                        or format == "DTS ES"
                        or format == "DTS ES XXCH"
                        or format == "DTS ES XXCH XLL"
                    ):
                        codec_tag = "DTS"
                    elif format == "MLP FBA 16-ch":
                        while i < len(output_lines):
                            if output_lines[i].startswith("Commercial name "):
                                commercial_name = output_lines[i].replace(
                                    "Commercial name                          : ", ""
                                )
                                if commercial_name == "Dolby TrueHD with Dolby Atmos":
                                    codec_tag = "Dolby TrueHD Atmos"
                                break
                            i = i + 1
                    else:
                        codec_tag = format
                i = i + 1
                if (codec_tag in codec_tags) == False:
                    if codec_tag == "V_MPEG4/ISO/AVC":
                        codec_tag = "H.264"
                    if codec_tag == "HEVC":
                        codec_tag = "H.265"
                    codec_tags += [codec_tag]
        if len(codec_tags) > 1 and codec_tags[0] == "UNKNOWN":
            codec_tags = codec_tags[1:]
        print("Computed tags are:")
        for codec_tag in codec_tags:
            print("-", codec_tag)
            if path_tags.find(codec_tag) == -1:
                if (
                    codec_tag == "PNG"
                    or codec_tag == "JPEG"
                    or codec_tag == "WebP"
                    or codec_tag == "PDF"
                    or codec_tag == "PGS"
                    or codec_tag == "UTF-8"
                    or codec_tag == "ASS"
                    or codec_tag == "VobSub"
                    or codec_tag == "DVB Subtitle"
                ) and (
                    ("FLAC" in codec_tags)
                    or ("Opus" in codec_tags)
                    or ("AAC" in codec_tags)
                    or ("MP3" in codec_tags)
                    or ("H.264" in codec_tags)
                    or ("H.265" in codec_tags)
                    or ("VP8" in codec_tags)
                    or ("XviD" in codec_tags)
                ):
                    # Ok, image codecs are not important because they come from cover or poster
                    pass
                else:
                    print(sin.term.tag_error(), 'Missing tag "' + codec_tag + '" in the tags')
    print("")

    # Check size
    sum_sizes = 0

    def compute_size(path):
        r = 0
        if os.path.isdir(path):
            filenames = os.listdir(path)
            for filename in filenames:
                r += compute_size(os.path.join(path, filename))
        else:
            r = os.path.getsize(path)
        return r

    for filename in filenames:
        file_size = compute_size(os.path.join(quiet_files_dir, path, filename))
        sum_sizes += file_size
    sum_sizes_unit = "o"
    while sum_sizes > 1024:
        if sum_sizes_unit == "o":
            sum_sizes_unit = "Kio"
        elif sum_sizes_unit == "Kio":
            sum_sizes_unit = "Mio"
        elif sum_sizes_unit == "Mio":
            sum_sizes_unit = "Gio"
        elif sum_sizes_unit == "Gio":
            sum_sizes_unit = "Tio"
        else:
            break
        sum_sizes /= 1024
    sum_sizes = round(sum_sizes, 2)
    print("Sum of sizes =", sum_sizes, sum_sizes_unit)
    if not (str(sum_sizes) + " " + sum_sizes_unit) in path:
        print(
            sin.term.tag_error(),
            'Missing tag "' + str(sum_sizes) + " " + sum_sizes_unit + '" in the tags',
        )

    # Check replay gain
    for filename in os.listdir(os.path.join(quiet_files_dir, path)):
        extension = filename.split(".")[-1]
        if len(filename.split(".")) == 1:
            extension = ""
        file_path = os.path.join(quiet_files_dir, path, filename)
        if extension == "flac" or extension == "ogg":
            ## Do not work all the time because grep can believe read a binary file
            # nb_replaygain_tag = int(subprocess.check_output('metaflac --list "' + file_path + '" | grep REPLAYGAIN | wc -l', shell=True))
            # if nb_replaygain_tag != 5:
            # print(sin.term.tag_error(), 'Missing some ReplayGain tags in "' + file_path + '"')
            for tag in [
                "REPLAYGAIN_REFERENCE_LOUDNESS",
                "REPLAYGAIN_TRACK_GAIN",
                "REPLAYGAIN_TRACK_PEAK",
                "REPLAYGAIN_ALBUM_GAIN",
                "REPLAYGAIN_ALBUM_PEAK",
            ]:
                nb_replaygain_tag = int(
                    subprocess.check_output(
                        "metaflac --show-tag=" + tag + ' "' + file_path + '" | wc -l',
                        shell=True,
                    )
                )
                if nb_replaygain_tag != 1:
                    print(
                        sin.term.tag_error(),
                        "Missing ReplayGain tag " + tag + ' in "' + file_path + '"',
                    )
                    print(
                        '  For flac, run "metaflac --add-replay-gain *.flac" in the directory'
                    )
        elif extension == "mp3" or extension == "m4a":
            exe_gain = "mp3gain" if extension == "mp3" else "aacgain"
            output = subprocess.check_output(
                exe_gain + ' -s c "' + file_path + '"', shell=True
            ).decode("utf-8")
            ok = True
            ok = ok and 'Recommended "Track" dB change: ' in output
            ok = ok and 'Recommended "Track" mp3 gain change: ' in output
            ok = ok and "Max mp3 global gain field: " in output
            ok = ok and "Min mp3 global gain field: " in output
            ok = ok and 'Recommended "Album" dB change: ' in output
            ok = ok and 'Recommended "Album" mp3 gain change: ' in output
            ok = ok and "Max Album PCM sample at current gain: " in output
            ok = ok and "Max Album mp3 global gain field: " in output
            ok = ok and "Min Album mp3 global gain field: " in output
            if ok == False:
                print(
                    sin.term.tag_error(), 'Missing some ReplayGain tags in "' + file_path + '"'
                )
                if extension == "mp3":
                    print('  For mp3, run "mp3gain *.mp3" in the directory')
                else:  # m4a
                    print('  For m4a, run "aacgain *.m4a" in the directory')
        elif extension == "opus":
            print(
                sin.term.tag_warning(),
                'opusgain is not supported (Do not check ReplayGain tags of "'
                + file_path
                + ')"',
            )
        elif extension in ["", "md", "jpg", "png", "webp", "mkv", "pdf", "zip"]:
            pass
        else:
            print(sin.term.tag_error(), 'Cannot check ReplayGain tags of "' + file_path + '"')
    print("")

    # Check .md
    print(sin.term.blue("Check keys"))
    print("")

    md = open(md_path).read()
    try:
        md_lines = md.split("\n")
        i = 0

        # Parse line
        def md_parse_line(i, line, key, key_display, required=True):
            r = ""
            if line.startswith(key):
                r = line[len(key) :]
            else:
                if required:
                    print(sin.term.tag_error(), "Line", i + 1, "of the .md must start by", key)
            if key_display != "" and (r != "" or required == False):
                if key_display == "Website":
                    if r.startswith("<") == False or r.endswith(">") == False:
                        print(
                            sin.term.tag_error(),
                            key_display,
                            "URL must be decored with <URL>",
                            "(Line" + str(i + 1) + ")",
                        )
                if r != "":
                    print("- " + key_display + ":", '"' + r + '"')
                else:
                    print("- " + sin.term.orange("NO `", key_display, "`"))
            return r

        # Parse empty line
        def md_parse_empty_line(i, line, required=True):
            if md_lines[i] != "" and required:
                print(sin.term.tag_error(), "Line", i + 1, "of the .md must be empty")
            print("")
            return line

        # Parse 'KEY (LANG)'
        def md_parse_keys_with_lang(i, lines, key, display=True, required=True):
            r = []
            if lines[i].startswith("`" + key + " (") == False and required:
                print(
                    sin.term.tag_error(),
                    "Line",
                    i + 1,
                    "of the .md must start by `" + key + " (LANG)`",
                )
            while lines[i].startswith("`" + key + " ("):
                if (lines[i][len(key) + 5] != ")") and (
                    lines[i][len(key) + 5] == "," and lines[i][len(key) + 10] != ")"
                ):
                    print(
                        sin.term.tag_error(), "Cannot parse `" + key + " (LANG)` at line ", i + 1
                    )
                else:
                    i_end_parenthese = lines[i][len(key) + 3 :].find(")")
                    r += [
                        (
                            lines[i][len(key) + 3 : len(key) + 3 + i_end_parenthese],
                            lines[i][len(key) + 3 + i_end_parenthese + 3 :],
                        )
                    ]
                i = i + 1
            # Print
            if len(r) == 0:
                print("- " + sin.term.orange("NO `", key, " (LANG)`"))
            for e in r:
                if e[0] == "":
                    print("- " + key + ":", '"' + e[1] + '"')
                else:
                    print("- " + key + " (" + e[0] + "):", '"' + e[1] + '"')
            return r

        def md_parse_titles(i, lines, display=True, required=True):
            return md_parse_keys_with_lang(i, lines, "Title", display, required)

        def md_parse_narrators(i, lines, display=True, required=True):
            return md_parse_keys_with_lang(i, lines, "Narrated by", display, required)

        def md_parse_casts(i, lines, display=True, required=True):
            return md_parse_keys_with_lang(i, lines, "Cast", display, required)

        def md_parse_voice_casts(i, lines, display=True, required=True):
            return md_parse_keys_with_lang(i, lines, "Voice cast", display, required)

        # Check .md title
        if md_lines[i] != "# " + path:
            print(sin.term.tag_error(), "Title of the .md is not the name of the directory")
        i = i + 1
        if md_lines[i] != "":
            print(sin.term.tag_error(), "Line", i + 1, "of the .md must be empty")
        i = i + 1
        titles_per_lang = []
        # Check .md `Title (LANG)` (Film*, Television show)
        if (
            type.startswith("Film")
            or type.startswith("Series")
            or type.startswith("Short film")
            or type.startswith("Solo performance")
            or type.startswith("Television show")
            or type.startswith("Theatre")
        ):
            titles = md_parse_titles(i, md_lines)
            i = i + len(titles)
        # Check .md `By` and `Title` (Music*)
        if type.startswith("Music"):
            if md_parse_line(i, md_lines[i], "`By` ", "By") != "":
                i = i + 1
            if md_parse_line(i, md_lines[i], "`Title` ", "Title") != "":
                i = i + 1
        # Check .md `Title` (Software*)
        if type.startswith("Software"):
            if md_parse_line(i, md_lines[i], "`Title` ", "Title") != "":
                i = i + 1
        # Check .md `Year`
        if md_parse_line(i, md_lines[i], "`Year` ", "Year") != "":
            i = i + 1
        # Check .md `Version` (Software*)
        if type.startswith("Software"):
            if md_parse_line(i, md_lines[i], "`Version` ", "Version") != "":
                i = i + 1
        if md_parse_empty_line(i, md_lines[i]) == "":
            i = i + 1

        # ---
        # Check .md `Type`,  `Country`, `Language` and `Genre`
        type = md_parse_line(i, md_lines[i], "`Type` ", "Type")
        if type != "":
            i = i + 1
        if md_parse_line(i, md_lines[i], "`Country` ", "Country") != "":
            i = i + 1
        if md_parse_line(i, md_lines[i], "`Language` ", "Language") != "":
            i = i + 1
        genre = md_parse_line(i, md_lines[i], "`Genre` ", "Genre")
        if genre != "":
            i = i + 1
        if md_parse_empty_line(i, md_lines[i]) == "":
            i = i + 1

        # ---
        # Check .md `Directed by`, `Produced by`, `Written by`, `Screenplay`, `Story by` and `Based on` (Film*, Television show)
        if (
            type.startswith("Film")
            or type.startswith("Series")
            or type.startswith("Short film")
            or type.startswith("Television show")
        ):
            if (
                md_parse_line(i, md_lines[i], "`Created by` ", "Created by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(i, md_lines[i], "`Directed by` ", "Directed by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(i, md_lines[i], "`Produced by` ", "Produced by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(i, md_lines[i], "`Written by` ", "Written by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(
                    i, md_lines[i], "`Screenplay by` ", "Screenplay by", False
                )
                != ""
            ):
                i = i + 1
            if md_parse_line(i, md_lines[i], "`Story by` ", "Story by", False) != "":
                i = i + 1
            if md_parse_line(i, md_lines[i], "`Based on` ", "Based on", False) != "":
                i = i + 1
            if (
                md_parse_line(
                    i, md_lines[i], "`Based on ... by` ", "Based on ... by", False
                )
                != ""
            ):
                i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
        if type.startswith("Theatre"):
            if (
                md_parse_line(i, md_lines[i], "`Written by` ", "Written by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(
                    i,
                    md_lines[i],
                    "`Theatre practitioner` ",
                    "Theatre practitioner",
                    False,
                )
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(i, md_lines[i], "`Recorded at` ", "Recorded at", False)
                != ""
            ):
                i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
        if type.startswith("Solo performance"):
            if (
                md_parse_line(i, md_lines[i], "`Written by` ", "Written by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(i, md_lines[i], "`Directed by` ", "Directed by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(i, md_lines[i], "`Produced by` ", "Produced by", False)
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(i, md_lines[i], "`Recorded at` ", "Recorded at", False)
                != ""
            ):
                i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1

        # ---
        # Check .md `Starring`, `Cast`, `Music by`, `Production company`, `Distributed by`
        if (
            type.startswith("Film")
            or type.startswith("Series")
            or type.startswith("Short film")
            or type.startswith("Solo performance")
            or type.startswith("Television show")
            or type.startswith("Theatre")
        ):
            if md_parse_line(i, md_lines[i], "`Starring` ", "Starring") != "":
                i = i + 1
            narrators = md_parse_narrators(i, md_lines, True, False)
            i = i + len(narrators)
            casts = md_parse_casts(i, md_lines, True, False)
            i = i + len(casts)
            voice_casts = md_parse_voice_casts(i, md_lines, True, False)
            i = i + len(voice_casts)
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
            if md_parse_line(i, md_lines[i], "`Music by` ", "Music by", False) != "":
                i = i + 1
                if md_parse_empty_line(i, md_lines[i]) == "":
                    i = i + 1
            if (
                md_parse_line(
                    i, md_lines[i], "`Production company` ", "Production company"
                )
                != ""
            ):
                i = i + 1
            if (
                md_parse_line(
                    i, md_lines[i], "`Distributed by` ", "Distributed by", False
                )
                != ""
            ):
                i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1

        # ---
        # Check .md `Studio`, `Label` and `Producer` (Music*)
        if type.startswith("Music"):
            studio_required = (
                (type != "Music, Film score")
                and (type != "Music, Soundtrack album")
                and (type != "Music, Live album")
                and (type != "Music, Greatest hits album")
                and (type != "Music, Music video")
                and (
                    path
                    != "Alexander Rybak (2014) Into a Fantasy (En) (1080p) [H.264, Opus, 42.55 Mio]"
                )
                and (path != "Alexander Rybak (2014) Into a Fantasy [Opus, 3.58 Mio]")
                and (
                    path
                    != "Soldat Louis (2010) V.I.P. - Very Intimes Poteaux [MP3, 147.28 Mio]"
                )
            )
            producer_required = (
                (type != "Music, Film score")
                and (type != "Music, Soundtrack album")
                and (type != "Music, Live album")
                and (type != "Music, Greatest hits album")
            )
            if (
                md_parse_line(i, md_lines[i], "`Studio` ", "Studio", studio_required)
                != ""
            ):
                i = i + 1
            if md_parse_line(i, md_lines[i], "`Label` ", "Label") != "":
                i = i + 1
            if (
                md_parse_line(
                    i, md_lines[i], "`Producer` ", "Producer", producer_required
                )
                != ""
            ):
                i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1

        # ---
        # Check .md `Released` (All) and  `Recorded` (Musics*)
        if md_parse_line(i, md_lines[i], "`Released` ", "Released") != "":
            i = i + 1
        if type.startswith("Music"):
            if md_parse_line(i, md_lines[i], "`Recorded` ", "Recorded", False) != "":
                i = i + 1
        if md_parse_empty_line(i, md_lines[i]) == "":
            i = i + 1

        # ---
        # Check .md `Directed by`, `Produced by`, `Written by`, `Screenplay`, `Story by` and `Based on` (Film*, Television show)
        if (
            type.startswith("Film")
            or type.startswith("Series")
            or type.startswith("Short film")
            or type.startswith("Solo performance")
            or type.startswith("Television show")
            or type.startswith("Theatre")
        ):
            if (
                md_parse_line(i, md_lines[i], "`Watch after` ", "Watch after", False)
                != ""
            ):
                i = i + 1
                if md_parse_empty_line(i, md_lines[i]) == "":
                    i = i + 1
            else:
                print("")

        # ---
        # Get serie titles, season titles, episodes titles
        if type.startswith("Series"):
            serie_titles = md_parse_titles(i, md_lines)
            i = i + len(serie_titles)
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
            season_titles = md_parse_keys_with_lang(
                i, md_lines, md_lines[i][1:].split()[0]
            )
            i = i + len(season_titles)
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
            while md_lines[i].startswith("`"):
                while md_lines[i].startswith("`"):
                    episode_titles = md_parse_keys_with_lang(
                        i, md_lines, md_lines[i][1:].split()[0]
                    )
                    i = i + len(episode_titles)
                if md_parse_empty_line(i, md_lines[i]) == "":
                    i = i + 1

        # ---
        # Check .md `Developed by`, `Website` (Software*)
        if type.startswith("Software"):
            if md_parse_line(i, md_lines[i], "`Developed by` ", "Developed by") != "":
                i = i + 1
            website = md_parse_line(i, md_lines[i], "`Website` ", "Website")
            if website != "":
                if website.startswith("<") == False or website.endswith(">") == False:
                    print(
                        sin.term.tag_error(),
                        "URL must be decored with <URL>",
                        "(Line" + str(i + 1) + ")",
                    )
                i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1

        # ---
        # Check .md `Designer`, `Music by` (Software*)
        if type.startswith("Software") and type.startswith("Game"):
            i_tmp = i
            designer = md_parse_line(i, md_lines[i], "`Designer` ", "Designer", False)
            if designer != "":
                i = i + 1
            music_by = md_parse_line(i, md_lines[i], "`Music by` ", "Music by", False)
            if music_by != "":
                i = i + 1
            if md_parse_empty_line(i, md_lines[i], i != i_tmp) == "":
                i = i + 1

        # ---
        while True:
            if md_lines[i].startswith("## File"):
                break
            # Check .md ## TITLE <URL> (LANG)
            empty_line_read = False
            in_subsection = False
            in_subsubsection = False
            if md_lines[i].startswith("## ") == False:
                print(sin.term.tag_error(), "Line", i + 1, "must be ## TITLE <URL> (LANG)")
                break
            else:
                print(sin.term.blue("Section") + ' "' + md_lines[i][3:] + '"')
                i = i + 1
                if md_parse_empty_line(i, md_lines[i]) == "":
                    i = i + 1
                while md_lines[i].startswith("## ") == False:
                    if md_lines[i] == "":
                        empty_line_read = True
                        print("")
                        i = i + 1
                    elif md_lines[i].startswith("### "):
                        print(
                            "  "
                            + sin.term.purple("Subsection")
                            + ' "'
                            + md_lines[i][4:]
                            + '"'
                        )
                        in_subsection = True
                        i = i + 1
                        if md_parse_empty_line(i, md_lines[i]) == "":
                            i = i + 1
                    elif md_lines[i].startswith("#### "):
                        print(
                            "    "
                            + sin.term.purple("Subsubsection")
                            + ' "'
                            + md_lines[i][5:]
                            + '"'
                        )
                        in_subsubsection = True
                        i = i + 1
                        if md_parse_empty_line(i, md_lines[i]) == "":
                            i = i + 1
                    else:
                        if in_subsubsection:
                            if len(md_lines[i]) > 41:
                                print(
                                    '    - "'
                                    + md_lines[i][0:40]
                                    + "[...]"
                                    + md_lines[i][-36:]
                                    + '"'
                                )
                            else:
                                print('    - "' + md_lines[i] + '"')
                        elif in_subsection:
                            if len(md_lines[i]) > 43:
                                print(
                                    '  - "'
                                    + md_lines[i][0:40]
                                    + "[...]"
                                    + md_lines[i][-38:]
                                    + '"'
                                )
                            else:
                                print('  - "' + md_lines[i] + '"')
                        else:
                            if len(md_lines[i]) > 45:
                                print(
                                    '- "'
                                    + md_lines[i][0:40]
                                    + "[...]"
                                    + md_lines[i][-40:]
                                    + '"'
                                )
                            else:
                                print('- "' + md_lines[i] + '"')
                        i = i + 1

        # ---
        # Check .md `## Files`
        files = []
        if i < len(md_lines) and md_lines[i].startswith("## Files"):
            print(sin.term.blue("Section") + ' "' + md_lines[i][3:] + '"')
            i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
            while (i < len(md_lines) and md_lines[i].startswith("`")) or (
                i + 1 < len(md_lines)
                and md_lines[i] == ""
                and md_lines[i + 1].startswith("`")
            ):
                if md_lines[i] == "":
                    md_parse_empty_line(i, md_lines[i])
                else:
                    print('- "' + md_lines[i][1:33] + '" "' + md_lines[i][36:-1] + '"')
                    files += [md_lines[i][36:-1]]
                i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
        else:
            print(sin.term.tag_error(), "Line", i + 1, "must be ## Files")

        # Check if cover, poster or screenshot is present
        cover_or_poster_or_screenshot_exist = False
        for file in files:
            if (
                (" - Cover (" in file)
                or (" - Cover - " in file)
                or (" - Cover 1 (" in file)
                or (" - Cover 1 - " in file)
                or (" - Cover." in file)
                or (" - Poster (" in file)
                or (" - Poster - " in file)
                or (" - Poster 1 (" in file)
                or (" - Poster 1 - " in file)
                or (" - Poster 1." in file)
                or (" - Poster." in file)
                or (" - Cover DVD - " in file)
                or (" - Screenshot." in file)
                or (" - Screenshot -" in file)
                or (" - Screenshot (" in file)
                or (" - Splash screen (" in file)
            ):
                cover_or_poster_or_screenshot_exist = True
        if cover_or_poster_or_screenshot_exist == False:
            if (
                path
                == "Ils s'aiment aussi (2017) (Fr) (1080p) (TVRIP France 2) [H.264, AC-3, 4.66 Gio]"
            ):
                pass  # Cover and poster not available
            else:
                print(sin.term.tag_error(), 'Cannot find "Cover", "Poster" or "Screenshot" file')
        # Check if trailer is present
        trailer_exist = False
        for file in files:
            if (" Trailer " in file) or (" Trailer." in file):
                trailer_exist = True
        if type.startswith("Film") and trailer_exist == False:
            if (
                path
                == "Les Mystères de Paris (1962) (Fr) (720p) [H.265, DTS, 2.79 Gio]"
                or path
                == "Les Mystères de Paris (1962) (Fr) (1080p) [H.264, AC-3, 7.2 Gio]"
            ):
                pass  # Trailer not available
            else:
                print(sin.term.tag_error(), 'Cannot find "Trailer" file')

        # ---
        # Check .md `## Logs`
        if i < len(md_lines) and md_lines[i] == "## Logs":
            print(sin.term.blue("Section") + ' "' + md_lines[i][3:] + '"')
            i = i + 1
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
            nb_logs = 0
            while i < len(md_lines) and md_lines[i].startswith("`"):
                date = md_lines[i][1:].split()[0][:-1]
                try:
                    datetime.datetime.strptime(date, "%Y-%m-%d")
                except Exception as e:
                    print(
                        sin.term.tag_error(),
                        "Line",
                        i + 1,
                        'must be start by `XXXX-XX-XX` and "'
                        + date
                        + '" is not a valid date',
                    )
                    print(e)
                log = md_parse_line(i, md_lines[i], "`" + date + "` ", date)
                if log != "":
                    if nb_logs == 0 and log != "Initial release":
                        print(
                            sin.term.tag_error(),
                            "Line",
                            i + 1,
                            ': First log must be "Initial release"',
                        )
                else:
                    print(sin.term.tag_error(), "Line", i + 1, ": log message cannot be empty")
                i = i + 1
                nb_logs += 1
            if nb_logs == 0:
                print(
                    sin.term.tag_error(), "Line", i + 1, "must be `XXXX-XX-XX` Initial release"
                )
            if md_parse_empty_line(i, md_lines[i]) == "":
                i = i + 1
        else:
            print(sin.term.tag_error(), "Line", i + 1, "must be ## Logs")

        # ---
        # End of file
        if i != len(md_lines):
            print(sin.term.tag_error(), "Line", i + 1, "should not exist")

    except Exception as e:
        print(sin.term.tag_error(), "Invalid .md file")
        print(e)


def list_all_filenames(path):
    r = []
    if os.path.isdir(path):
        filenames = os.listdir(path)
        for filename in filenames:
            r += list_all_filenames(os.path.join(path, filename))
    else:
        r = [path]
    return r


def quiet_files_check_md5(quiet_files_dir, path, md_path):
    # filenames = os.listdir(os.path.join(quiet_files_dir, path))
    filenames = sorted(list_all_filenames(os.path.join(quiet_files_dir, path)))
    filenames.remove(os.path.join(quiet_files_dir, path, path + ".md"))
    filenames_missing = filenames.copy()
    md_file = open(md_path, "r")
    print("")

    print(sin.term.blue("Check MD5"))
    print("")

    error = False
    for line in md_file:
        if len(line) >= 35 and line[0] == "`" and line[33] == "`":
            md5 = line[1:33]
            filename = line[36:-2]
            filename_path = os.path.join(quiet_files_dir, path, filename)
            if os.path.exists(filename_path):
                filenames_missing.remove(filename_path)
                print('- "' + filename + '"')
                new_md5 = sin.fs.md5(filename_path)
                if new_md5 != md5:
                    print(
                        "  " + sin.term.tag_error(),
                        'md5 is "' + new_md5 + '" instead of "' + md5 + '"',
                    )
                    error = True
            else:
                print("  " + sin.term.tag_error(), 'file "' + filename + '" does not exist.')
                error = True
    md_file.close()
    for filename_path in filenames_missing:
        print(sin.term.tag_error(), 'file "' + filename_path + '" has no md5 in the .md file')
        error = True
    if error:
        print("Expected MD5 are:")
        for filename_path in filenames:
            filename = filename_path[len(os.path.join(quiet_files_dir, path)) + 1 :]
            print("`" + sin.fs.md5(filename_path) + "`", "`" + filename + "`")


def quiet_files_create_links(quiet_files_dir, path, md_path):
    full_path = os.path.join(quiet_files_dir, path)

    for md_type in md_get_values_of_key(md_path, "Type"):

        md_types = [md_type]
        if md_type.startswith("Music, "):
            md_types += ["Music"]

        for type in md_types:

            if not os.path.exists(type):
                os.mkdir(type)
            if not os.path.exists(os.path.join(type, "All")):
                os.mkdir(os.path.join(type, "All"))
            print("- create link to " + '"' + os.path.join(type, "All") + '"')
            sin.fs.symlink(full_path, os.path.join(os.path.join(type, "All"), path))

            for genre in md_get_values_of_key(md_path, "Genre"):
                if not os.path.exists(os.path.join(type, "By Genre")):
                    os.mkdir(os.path.join(type, "By Genre"))
                if not os.path.exists(os.path.join(type, "By Genre", genre)):
                    os.mkdir(os.path.join(type, "By Genre", genre))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "By Genre", genre)
                    + '"'
                )
                sin.fs.symlink(
                    full_path, os.path.join(os.path.join(type, "By Genre", genre), path)
                )

            for year in md_get_values_of_key(md_path, "Year"):
                if not os.path.exists(os.path.join(type, "By Year")):
                    os.mkdir(os.path.join(type, "By Year"))
                if not os.path.exists(os.path.join(type, "By Year", year)):
                    os.mkdir(os.path.join(type, "By Year", year))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "By Year", year)
                    + '"'
                )
                sin.fs.symlink(
                    full_path, os.path.join(os.path.join(type, "By Year", year), path)
                )

            for country in md_get_values_of_key(md_path, "Country"):
                if not os.path.exists(os.path.join(type, "By Country")):
                    os.mkdir(os.path.join(type, "By Country"))
                if not os.path.exists(os.path.join(type, "By Country", country)):
                    os.mkdir(os.path.join(type, "By Country", country))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "By Country", country)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(os.path.join(type, "By Country", country), path),
                )

            for lang in md_get_values_of_key(md_path, "Language"):
                if not os.path.exists(os.path.join(type, "By Language")):
                    os.mkdir(os.path.join(type, "By Language"))
                if not os.path.exists(os.path.join(type, "By Language", lang)):
                    os.mkdir(os.path.join(type, "By Language", lang))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "By Language", lang)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(os.path.join(type, "By Language", lang), path),
                )

            # for title_en in md_get_values_of_key(md_path, 'Title (En)'):
            # if not os.path.exists(os.path.join(type, 'By Title (En)')): os.mkdir(os.path.join(type, 'By Title (En)'))
            # if not os.path.exists(os.path.join(type, 'By Title (En)', title_en)): os.mkdir(os.path.join(type, 'By Title (En)', title_en))
            # print('- create link to ' + '"' + os.path.join(type, 'By Title (En)', title_en) + '"')
            # sin.fs.symlink(full_path, os.path.join(os.path.join(type, 'By Title (En)', title_en), path))

            # for title_fr in md_get_values_of_key(md_path, 'Title (Fr)'):
            # if not os.path.exists(os.path.join(type, 'By Title (Fr)')): os.mkdir(os.path.join(type, 'By Title (Fr)'))
            # if not os.path.exists(os.path.join(type, 'By Title (Fr)', title_fr)): os.mkdir(os.path.join(type, 'By Title (Fr)', title_fr))
            # print('- create link to ' + '"' + os.path.join(type, 'By Title (Fr)', title_fr) + '"')
            # sin.fs.symlink(full_path, os.path.join(os.path.join(type, 'By Title (Fr)', title_fr), path))

            # for title_jp in md_get_values_of_key(md_path, 'Title (Jp)'):
            # if not os.path.exists(os.path.join(type, 'By Title (Jp)')): os.mkdir(os.path.join(type, 'By Title (Jp)'))
            # if not os.path.exists(os.path.join(type, 'By Title (Jp)', title_jp)): os.mkdir(os.path.join(type, 'By Title (Jp)', title_jp))
            # print('- create link to ' + '"' + os.path.join(type, 'By Title (Jp)', title_jp) + '"')
            # sin.fs.symlink(full_path, os.path.join(os.path.join(type, 'By Title (Jp)', title_jp), path))

            for directed_by in md_get_values_of_key(md_path, "Directed by"):
                if not os.path.exists(os.path.join(type, "Directed by")):
                    os.mkdir(os.path.join(type, "Directed by"))
                if not os.path.exists(os.path.join(type, "Directed by", directed_by)):
                    os.mkdir(os.path.join(type, "Directed by", directed_by))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "Directed by", directed_by)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(os.path.join(type, "Directed by", directed_by), path),
                )

            for produced_by in md_get_values_of_key(md_path, "Produced by"):
                if not os.path.exists(os.path.join(type, "Produced by")):
                    os.mkdir(os.path.join(type, "Produced by"))
                if not os.path.exists(os.path.join(type, "Produced by", produced_by)):
                    os.mkdir(os.path.join(type, "Produced by", produced_by))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "Produced by", produced_by)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(os.path.join(type, "Produced by", produced_by), path),
                )

            for screenplay_by in md_get_values_of_key(md_path, "Screenplay by"):
                if not os.path.exists(os.path.join(type, "Screenplay by")):
                    os.mkdir(os.path.join(type, "Screenplay by"))
                if not os.path.exists(
                    os.path.join(type, "Screenplay by", screenplay_by)
                ):
                    os.mkdir(os.path.join(type, "Screenplay by", screenplay_by))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "Screenplay by", screenplay_by)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(
                        os.path.join(type, "Screenplay by", screenplay_by), path
                    ),
                )

            for story_by in md_get_values_of_key(md_path, "Story by"):
                if not os.path.exists(os.path.join(type, "Story by")):
                    os.mkdir(os.path.join(type, "Story by"))
                if not os.path.exists(os.path.join(type, "Story by", story_by)):
                    os.mkdir(os.path.join(type, "Story by", story_by))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "Story by", story_by)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(os.path.join(type, "Story by", story_by), path),
                )

            for written_by in md_get_values_of_key(md_path, "Written by"):
                if not os.path.exists(os.path.join(type, "Written by")):
                    os.mkdir(os.path.join(type, "Written by"))
                if not os.path.exists(os.path.join(type, "Written by", written_by)):
                    os.mkdir(os.path.join(type, "Written by", written_by))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "Written by", written_by)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(os.path.join(type, "Written by", written_by), path),
                )

            for music_by in md_get_values_of_key(md_path, "Music by"):
                if not os.path.exists(os.path.join(type, "Music by")):
                    os.mkdir(os.path.join(type, "Music by"))
                if not os.path.exists(os.path.join(type, "Music by", music_by)):
                    os.mkdir(os.path.join(type, "Music by", music_by))
                print(
                    "- create link to "
                    + '"'
                    + os.path.join(type, "Music by", music_by)
                    + '"'
                )
                sin.fs.symlink(
                    full_path,
                    os.path.join(os.path.join(type, "Music by", music_by), path),
                )


def is_img(path):
    if path.endswith(".jpg") or path.endswith(".png") or path.endswith(".webp"):
        return True
    return False


def get_first_img_lang(filenames, lang):
    for filename in filenames:
        if lang == None or ("(" + lang + ")" in filename):
            if is_img(filename):
                return filename
    return None


def get_first_img(filenames):
    for lang in ["En", "Fr", None]:
        r = get_first_img_lang(filenames, lang)
        if r != None:
            return r
    raise Exception("Cannot find image from", filenames)


html = """<!-- {path} -->
<div style="width:300px;height:300px+3em;display:inline-block;text-align:center;vertical-align:top;margin:10px">
  <a href="../{path}">
    <img with={img_width} height={img_height} src="{{data_dir}}/{thumbnail_filename}" alt="{path}"><br>
  </a>
  {title}<br>
  {year} {langs}<br>
  <tiny>{tags}</tiny>
</div>
"""


def quiet_files_create_site(quiet_files_dir, path, md_path, lang):
    full_path = os.path.join(quiet_files_dir, path)

    md_types = md_get_values_of_key(md_path, "Type")
    if md_types[0].startswith("Music, "):
        md_types += ["Music"]

    for md_type in md_types:

        if True:  # md_type == 'Film' or md_type == 'Series':

            output_dir = "md2website-data-qf"
            data_output_dir = os.path.join(output_dir, "data")

            filenames = sorted(os.listdir(full_path))
            print("-", filenames)

            # thumbnail

            main_img_filename = get_first_img(filenames)
            main_img_path = os.path.join(full_path, main_img_filename)
            thumbnail_filename = "thumbnail_" + path + ".jpg"
            thumbnail_path = os.path.join(data_output_dir, thumbnail_filename)
            print("- create thumbnail", thumbnail_path, "from", main_img_path)

            img = Image.open(main_img_path)
            img.thumbnail((300, 300))
            img.save(thumbnail_path)

            # page

            page_path = os.path.join(output_dir, path + "." + lang + ".md")
            print("- create page", page_path)

            with open(page_path, "a") as f:

                f.write("# " + path + "\n\n")

                for filename in filenames:
                    if filename.endswith(".mkv"):
                        video_path = os.path.join("{data_dir}", full_path, filename)
                        f.write('<a href="' + video_path + '">' + filename + "</a>  \n")
                f.write("\n")

                f.write("# Images\n\n")

                for filename in filenames:
                    if is_img(filename):
                        img_path = os.path.join("{data_dir}", full_path, filename)
                        f.write(
                            '<img height=300 src="'
                            + img_path
                            + '" alt="'
                            + filename
                            + '"> \n'
                        )
                f.write("\n")

                f.write("# Other Files\n\n")

                for filename in filenames:
                    if not filename.endswith(".mkv") and not is_img(filename):
                        file_path = os.path.join("{data_dir}", full_path, filename)
                        f.write('<a href="' + file_path + '">' + filename + "</a>  \n")
                f.write("\n")

            # index.html

            list_page_path = os.path.join(output_dir, md_type + "." + lang + ".md")
            print('- append "' + list_page_path + '"')

            i_tags = path.rfind("(")
            tags = path[i_tags:]
            title_year_langs = path[:i_tags]

            i_langs = title_year_langs.rfind("(")
            langs = title_year_langs[i_langs + 1 : -2]
            title_year = title_year_langs[: i_langs - 1]

            i_year = title_year.rfind("(")
            year = title_year[i_year + 1 : -1]
            title = title_year[: i_year - 1]

            with open(list_page_path, "a") as f:
                f.write(
                    html.format(
                        path=path,
                        img_width=str(img.width),
                        img_height=str(img.height),
                        thumbnail_filename=thumbnail_filename,
                        title=title,
                        year=year,
                        langs=langs,
                        tags=tags,
                    )
                )


def quiet_files_chmod(quiet_files_dir, path, indent=0):
    full_path = os.path.join(quiet_files_dir, path)
    filenames = os.listdir(full_path)
    for filename in filenames:
        if os.path.isfile(os.path.join(full_path, filename)):
            print((" " * indent) + '- chmod a=r "' + filename + '"')
            command = 'chmod a=r "' + os.path.join(full_path, filename) + '"'
            os.system(command)
        elif os.path.isdir(os.path.join(full_path, filename)):
            print((" " * indent) + '- chmod a=rx "' + filename + '"')
            command = 'chmod a=rx "' + os.path.join(full_path, filename) + '"'
            os.system(command)
            quiet_files_chmod(full_path, filename, indent + 2)


def quiet_files_select_musics(quiet_files_dir, path, select_musics_data):
    full_path = os.path.join(quiet_files_dir, path)
    filenames = os.listdir(full_path)
    i_data_lines = []
    # Replace path
    for filename in sorted(filenames):
        i_data_line = 0
        for data_line in select_musics_data:
            for data_filename in data_line[1]:
                i_data_filename = 0
                if data_filename in filename:
                    # print('- Select "' + filename + '" to replace "' + data_filename + '"')
                    data_line[1][i_data_filename] = filename
                    i_data_lines += [i_data_line]
                i_data_filename += 1
            i_data_line += 1
    # Process
    # if len(i_data_lines) != 0:
    # print(sin.term.blue('Copy musics:'))
    for i_data_line in i_data_lines:
        data_line = select_musics_data[i_data_line]
        output_dir = data_line[0]
        filenames = data_line[1]
        # TODO: Handle several filenames and times
        print('- Copy "' + filenames[0] + '" in "' + output_dir + '" directory')
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        shutil.copy(
            os.path.join(full_path, filenames[0]),
            os.path.join(output_dir, filenames[0]),
        )
        # sin.fs.symlink(os.path.join(full_path, filenames[0]), os.path.join(output_dir, filenames[0]))


def quiet_files_zip_extra_dir(quiet_files_dir, path):
    print("")
    print(sin.term.blue('Create ".extra.zip"'))
    print("")

    full_path = os.path.join(quiet_files_dir, path)
    dot_extra_full_path = os.path.join(full_path, ".extra")
    if os.path.exists(dot_extra_full_path) == False:
        print(sin.term.tag_warning(), '".extra" directory does not exist')
        return
    if os.path.isdir(dot_extra_full_path) == False:
        print(sin.term.tag_warning(), '".extra" directory exists but is not a directory')
        return

    work_dir = os.getcwd()
    os.chdir(full_path)

    # Concerning this https://stackoverflow.com/questions/21988059/error-encoding-zipfile
    # my_zipfile = zipfile.ZipFile('.extra.zip', mode='w', compression=zipfile.ZIP_DEFLATED, compresslevel=9)
    # dirs = [ '.extra' ]
    # while len(dirs) != 0:
    # dir = dirs[0]
    # dirs = dirs[1:]
    # for filename in os.listdir(dir):
    # if filename == '.' or filename == '..': continue
    # path = os.path.join(dir, filename)
    # if os.path.isdir(path):
    # dirs += [ path ]
    # else:
    # print('- "' + path + '"')
    # my_zipfile.write(path)
    # my_zipfile.close()
    # print('')

    # I prefer to use zip command
    cmd = "zip -r -9 .extra.zip .extra"
    print(cmd)
    os.system(cmd)
    print("")

    print('Rename ".extra" into "_DOT_EXTRA_DIR_TO_BE_DELETED"')
    os.rename(".extra", "_DOT_EXTRA_DIR_TO_BE_DELETED")

    os.chdir(work_dir)


def quiet_files_create_torrent(quiet_files_dir, path):
    full_path = os.path.join(quiet_files_dir, path)
    output_filename = path + ".torrent"
    output_filename = output_filename.replace("ô", "o")
    output_filename = output_filename.replace("Ô", "O")
    print('- create torrent for "' + full_path + '"')
    command = "qf_create_torrent"
    command += " " + '"' + path + '"'
    command += " " + "-a https://tr4ck3r.bagneres.org/announce"
    command += " " + '-o "' + output_filename + '"'
    print('run "' + command + '"')
    work_dir = os.getcwd()
    os.chdir(quiet_files_dir)
    os.system(command)
    os.chdir(work_dir)


def quiet_files_create_nfo(quiet_files_dir, path, md_path):
    filenames = os.listdir(os.path.join(quiet_files_dir, path))
    try:
        filenames.remove(path + ".md")
    except:
        pass
    for filename in filenames:
        filename_path = os.path.join(quiet_files_dir, path, filename)
        if filename == "nfo":
            if os.path.isdir(filename_path) == False:
                print("  " + sin.term.tag_error(), "nfo file is not a directory")
            filenames.remove(filename)
        # if os.path.isdir(filename_path):
        # filenames.remove(filename)
    if len(filenames) > 0:
        try:
            os.mkdir(os.path.join(quiet_files_dir, path, "nfo"))
        except:
            pass
    print("---")
    print("Create .nfo:")
    for filename in filenames:
        print('- "' + filename + '"')
        dirname = os.path.join(quiet_files_dir, path)
        filename_nfo_path = (
            os.path.join(quiet_files_dir, path, "nfo", filename) + ".nfo"
        )
        if os.path.exists(filename_nfo_path):
            print(
                sin.term.tag_error(),
                'file "' + filename_nfo_path + '" already exists, skip this file',
            )
            continue
        command = (
            'cd "'
            + dirname
            + '" && mediainfo "'
            + filename
            + '" > "../../'
            + filename_nfo_path
            + '"'
        )
        # print('run "' + command + '"')
        os.system(command)


def quiet_files_reencode_flac(quiet_files_dir, path):
    full_path = os.path.join(quiet_files_dir, path)
    first = True
    for filename in sorted(os.listdir(full_path)):
        extension = filename.split(".")[-1]
        if extension == "flac":
            if first:
                print('Convert .flac to .flac with "--best -p -e" options')
                first = False
            print("-", filename)
            file_full_path_new = os.path.join(full_path, filename)
            file_full_path_old = os.path.join(full_path, filename[:-5] + "_.flac")
            os.rename(file_full_path_new, file_full_path_old)
            command = (
                'flac --best -p -e "'
                + file_full_path_old
                + '"'
                + ' -o "'
                + file_full_path_new
                + '"'
            )
            os.system(command)


def quiet_files_cue_flac_to_flac(quiet_files_dir, path):
    full_path = os.path.join(quiet_files_dir, path)
    command = (
        'cd "'
        + full_path
        + '" && shnsplit -f *.cue -t "%p (YEAR) %a - %n. %t" -o flac *.flac'
    )
    print(sin.term.blue(command))
    os.system(command)


def quiet_files(quiet_files_dir, path, operations):

    directory_path = os.path.join(quiet_files_dir, path)

    if os.path.isdir(directory_path):

        print(sin.term.green('In directory "' + directory_path + '"'))

        md_path = os.path.join(directory_path, path) + ".md"
        md_path_exist = os.path.exists(md_path)

        for operation in operations:

            if operation == "check_tag":
                if md_path_exist == False:
                    print(
                        sin.term.tag_error(),
                        '.md file "'
                        + md_path
                        + '" does not exist, cannot execute "check_tag" operation',
                    )
                else:
                    quiet_files_check_tag(quiet_files_dir, path, md_path)

            elif operation == "check_md5":
                if md_path_exist == False:
                    print(
                        sin.term.tag_error(),
                        '.md file "'
                        + md_path
                        + '" does not exist, cannot execute "check_md5" operation',
                    )
                else:
                    quiet_files_check_md5(quiet_files_dir, path, md_path)

            elif operation == "create_links":
                if md_path_exist == False:
                    print(
                        sin.term.tag_error(),
                        '.md file "'
                        + md_path
                        + '" does not exist, cannot execute "create_links" operation',
                    )
                else:
                    quiet_files_create_links(quiet_files_dir, path, md_path)

            elif operation == "chmod":
                quiet_files_chmod(quiet_files_dir, path)

            elif operation == "gen_md2website":
                if md_path_exist == False:
                    print(
                        sin.term.tag_error(),
                        '.md file "'
                        + md_path
                        + '" does not exist, cannot execute "create_links" operation',
                    )
                else:
                    for lang in ["en", "fr", "ru"]:
                        quiet_files_create_site(quiet_files_dir, path, md_path, lang)

            elif operation == "select_musics":
                quiet_files_select_musics(quiet_files_dir, path, qf_select_musics.data)

            elif operation == "zip_extra_dir":
                quiet_files_zip_extra_dir(quiet_files_dir, path)

            elif operation == "create_torrents":
                if md_path_exist == False:
                    print(
                        sin.term.tag_warning(),
                        '.md file "'
                        + md_path
                        + '" does not exist, "create_torrents" operation is too early?',
                    )
                quiet_files_create_torrent(quiet_files_dir, path)

            elif operation == "create_nfo":
                quiet_files_create_nfo(quiet_files_dir, path, md_path)

            elif operation == "reencode_flac":
                quiet_files_reencode_flac(quiet_files_dir, path)

            elif operation == "cue_flac_to_flac":
                quiet_files_cue_flac_to_flac(quiet_files_dir, path)

            else:
                print(sin.term.tag_error(), "Unknow operation", operation)

        print()


def gen_md2website():
    output_dir = "md2website-data-qf"
    data_output_dir = os.path.join(output_dir, "data")

    if os.path.exists(output_dir):
        print("Remove", output_dir, "directory")
        shutil.rmtree(output_dir)

    print("Create", output_dir, "directory")
    os.makedirs(output_dir)

    print("Copy md2website files")
    shutil.copy2("md2website-data.py", os.path.join(output_dir, "data.py"))
    shutil.copy2("md2website-index.en.md", os.path.join(output_dir, "index.en.md"))
    shutil.copy2("md2website-index.fr.md", os.path.join(output_dir, "index.fr.md"))
    shutil.copy2("md2website-index.ru.md", os.path.join(output_dir, "index.ru.md"))

    print("Create", data_output_dir, "directory")
    os.makedirs(data_output_dir)

    symlink_all = os.path.join(data_output_dir, "All")
    print('Create symlink "' + symlink_all + '"')
    sin.fs.symlink("All", symlink_all)


# Main

if __name__ == "__main__":

    print(sin.term.blue("Check is mediainfo is installed"))
    if os.system("mediainfo --version") != 0:
        print(sin.term.tag_error(), "mediainfo is not installed")
        print('- please install the package "mediainfo"')
    print()

    print(sin.term.blue("Check is metaflac is installed"))
    if os.system("metaflac --version") != 0:
        print(sin.term.tag_error(), "metaflac is not installed")
        print('- please install the package "flac"')
    print()

    print(sin.term.blue("Check is mp3gain is installed"))
    if os.system("mp3gain -v") != 0:
        print(sin.term.tag_error(), "mp3gain is not installed")
        print('- please install the package "mp3gain"')
    print()

    print(sin.term.blue("Check is aacgain is installed"))
    if os.system("aacgain -v") != 0:
        print(sin.term.tag_error(), "aacgain is not installed")
        print("- please install aacgain:")
        print("  cd /tmp")
        print(
            "  wget https://www.deb-multimedia.org/pool/main/a/aacgain/aacgain_1.9-dmo4_amd64.deb"
        )
        print("  su -")
        print("  cd /tmp")
        print("  dpkg -i aacgain_1.9-dmo4_amd64.deb")
        print("  apt install")
    print()

    print(sin.term.blue("Check is shnsplit is installed"))
    if os.system("shnsplit -v") != 0:
        print(sin.term.tag_error(), "shnsplit is not installed")
        print('- please install the packages "cuetools shntool"')
        print()

    print(sin.term.blue("Check is qf_create_torrent is installed"))
    if os.system("qf_create_torrent -v") != 0:
        print(sin.term.tag_error(), "qf_create_torrent is not installed")
        print("- please install qf_create_torrent:")
        print('  install packages "build-essential libboost-all-dev libssl-dev"')
        print("  cd /tmp")
        print(
            "  wget https://github.com/arvidn/libtorrent/archive/libtorrent_1_2_0.zip"
        )
        print("  unzip libtorrent_1_2_0.zip")
        print("  cd libtorrent-libtorrent_1_2_0")
        print("  mkdir build")
        print("  cd build")
        print("  cmake ..")
        print("  make -j 4")
        print('  su -c "make install"')
        print('  cd "' + os.path.dirname(os.path.realpath(__file__)) + '"')
        print(
            "  g++ -std=c++17 -pedantic -Wall -Wextra -Wconversion -Wsign-conversion qf_create_torrent.cpp -o qf_create_torrent -Wl,-rpath,/usr/local/lib -ltorrent-rasterbar -lboost_system"
        )
        print('  su -c "cp qf_create_torrent /usr/local/bin"')
    print()

    print(sin.term.green(software + " " + version))
    print("Script path =", os.path.realpath(__file__))
    print("Current directory =", os.getcwd())
    print("Args =", sys.argv)
    print()

    if len(sys.argv) < 3:
        print(
            sin.term.orange("Usage:"),
            sys.argv[0],
            "<directory where are quiet files>",
            "<option>",
        )
        print()
        print("- Check options:")
        print()
        print("  " + "check_tag          Check tags")
        print("  " + "check_md5          Check files md5")
        print()
        print("- User options:")
        print()
        print(
            "  "
            + "create_links       Create the symlinks to navigate into the database"
        )
        print("  " + "chmod              Run chmod a-w for all files")
        print("  " + "gen_md2website     Generate md2website data")
        print()
        print("- User options for musics:")
        print()
        print(
            "  "
            + 'select_musics      Select musics according "qf_select_musics.py" file'
        )
        print("  " + "transcode_mp3      Transcode musics to MP3")
        print()
        print("- Create options:")
        print()
        print("  " + "zip_extra_dir      Zip and rename .extra directory")
        print("  " + "create_torrents    Create the torrents")
        print(
            "  "
            + "create_nfo         DEPRECATED: Create a .nfo file with mediainfo for each file"
        )
        print()
        print("- Work options:")
        print()
        print(
            "  " + "reencode_flac      Reencode all flac to have the best compression"
        )
        print(
            "  "
            + "cue_flac_to_flac   Transform one .cue + .flac files into several .flac files"
        )
        print()
        sys.exit(1)

    quiet_files_dir = sys.argv[1]
    operations = sys.argv[2:]
    print("Directory where are quiet files =", quiet_files_dir)
    print("Operation =", operations)
    print()

    if "select_musics" in operations:
        sys.path.append(os.getcwd())
        import qf_select_musics

    if "gen_md2website" in operations:
        gen_md2website()
        print()

    print(sin.term.green('In directory "' + quiet_files_dir + '"'))
    if "chmod" in operations:
        quiet_files_chmod(quiet_files_dir, "")
    print()

    if len(operations) != 0:
        for path in sorted(os.listdir(quiet_files_dir)):
            quiet_files(quiet_files_dir, path, operations)
